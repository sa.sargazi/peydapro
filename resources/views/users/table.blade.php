<table class="table table-responsive" id="users-table">
    <thead>
        <tr>
            <th>First Name</th>
        <th>Last Name</th>
        <th>Role</th>
        <th>National Code</th>
        <th>Mobile</th>
        <th>Email</th>
        <th>State</th>
        <th>City</th>
        <th>Phone</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr>
            <td>{!! $user->first_name !!}</td>
            <td>{!! $user->last_name !!}</td>
            <td>{!! $user->role !!}</td>
            <td>{!! $user->national_code !!}</td>
            <td>{!! $user->mobile !!}</td>
            <td>{!! $user->email !!}</td>
            <td>{!! $user->state !!}</td>
            <td>{!! $user->city !!}</td>
            <td>{!! $user->phone !!}</td>
            <td>
                {!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('users.show', [$user->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('users.edit', [$user->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>