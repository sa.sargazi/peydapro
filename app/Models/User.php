<?php

namespace App\Models;

use Eloquent as Model;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


/**
 * Class User
 * @package App\Models
 * @version August 4, 2018, 6:39 am UTC
 *
 * @property string first_name
 * @property string last_name
 * @property string role
 * @property string national_code
 * @property string mobile
 * @property string email
 * @property integer state
 * @property integer city
 * @property string phone
 */
class User extends Authenticatable implements JWTSubject
{
    use SoftDeletes;
    use Notifiable;

    public $table = 'users';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'first_name',
        'last_name',
        'role',
        'national_code',
        'mobile',
        'email',
        'state',
        'city',
        'phone',
        'token',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'first_name' => 'string',
        'last_name' => 'string',
        'role' => 'string',
        'national_code' => 'string',
        'mobile' => 'string',
        'email' => 'string',
        'state' => 'integer',
        'city' => 'integer',
        'phone' => 'string'
    ];

    //Rules for every user
    /**
     * Validation rules
     *
     * @var array
     */
    public static $updateRules = [
        'first_name' => 'required',
        'last_name' => 'required',
        //'national_code' => 'required|min:10|max:11|unique:users',
        //'mobile' => 'required|min:11|max:12|unique:users',
        'email' => 'required|email|unique:users',
        'state' => 'required',
        'city' => 'required'
    ];

    public static $mobileRules = [
        'mobile' => 'required|min:11|max:12',
    ];

    public static $OTPMobileRules = [
        'mobile' => 'required|min:11|max:12',
        'otp'=>'required|max:4|min:3'
    ];

    public static $storeRules = [

        'national_code' => 'required|min:10|max:11',
        'mobile' => 'required|min:11|max:12',

        ];

    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
    
}
