<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateUserAPIRequest;
use App\Http\Requests\API\LoginRequest;
use App\Http\Requests\API\OTPGenerateRequest;
use App\Http\Requests\API\OtpRequest;
use App\Http\Requests\API\UpdateUserAPIRequest;
use App\Http\Resources\UsersResource;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class UserAPIController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    /**
     * Display a listing of the User.
     * GET|HEAD /users
     *
     * @param Request $request
     * @return Response
     */
    public function index()
    {

        $this->userRepository->pushCriteria(new RequestCriteria($request));
        $this->userRepository->pushCriteria(new LimitOffsetCriteria($request));
        $users = $this->userRepository->all();

        return $this->sendResponse($users->toArray(), 'Users retrieved successfully');
    }

    /**
     * Store a newly created User in storage.
     * POST /users
     *
     * @param CreateUserAPIRequest $request
     *
     * @return Response
     */

    public function sendOtp(OTPGenerateRequest $request)
    {

        $digits = 4;
        $otp = rand(pow(10, $digits-1), pow(10, $digits)-1);
        Cache::put('mobileNumber-'.$request['mobile'],$otp, 10);
        //dd(Cache::get('mobile-otp'));
        return $this->sendResponse(["otp"=>$otp,"mobile"=>$request['mobile']], 'OTP send successfully');
    }

    public function getClientOtp(OtpRequest $request)
    {

        $mobile = $request['mobile'];

        $otp = $request['otp'];

        if(Cache::get("mobileNumber-".$request['mobile']) == $otp){

            $user = $this->userRepository->checkMobileExists($mobile);


            if($user){

                if($token =  $this->guard()->fromuser($user)){

                    $this->userRepository->update(['token'=>$token],$user->id);

                    return $this->sendResponse(new UsersResource($user,$token), 'Token sent successfully');

                }

            }
            else{

                return $this->sendError("mobile number have not registered yet", 503);
            }

        }

        return $this->sendError("mobile number and OTP doesnt match", 422);



    }



    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    public function guard()
    {
        return Auth::guard();
    }


    public function store(CreateUserAPIRequest $request)
    {

        $input = $request->all();

        $user = $this->userRepository->checkMobileExists($request['mobile']);

        if(!$user){

            $users = $this->userRepository->create($input);

            if($token =  $this->guard()->fromuser($users)){

                $this->userRepository->update(['token'=>$token],$users->id);

                return $this->sendResponse(new UsersResource($users,$token), 'Token sent successfully');

            }
        }


    }

    /**
     * Display the specified User.
     * GET|HEAD /users/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var User $user */
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        return $this->sendResponse($user->toArray(), 'User retrieved successfully');
    }

    /**
     * Update the specified User in storage.
     * PUT/PATCH /users/{id}
     *
     * @param  int $id
     * @param UpdateUserAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserAPIRequest $request)
    {
        //$user = JWTAuth::parseToken()->authenticate();

        $input = $request->all();

        /** @var User $user */
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        $user = $this->userRepository->update($input, $id);

        return $this->sendResponse($user->toArray(), 'User updated successfully');
    }

    /**
     * Remove the specified User from storage.
     * DELETE /users/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var User $user */
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        $user->delete();

        return $this->sendResponse($id, 'User deleted successfully');
    }
}
